<?php
include 'include/connection.php';



if(isset($_POST['logIn'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];

    if($auth->login($username,$password)){

        header('Location: index.php');
        exit;

    } else {
        $_SESSION['error'] = 'Wrong username or password!';
        header('Location: index.php');
        exit;
    }

}

?>