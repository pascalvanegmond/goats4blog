<div class="pos-f-t">
    <div class="blog-masthead">
        <div class="container">
            <nav class="nav blog-nav">
                <a class="nav-link active" id="home" href="/">Home</a>
                <a class="nav-link" id="post" href="/post/new.php">New post</a>
                <a class="nav-link" href="#">About</a>
                <?php if(isset($_SESSION['loggedin'])){ ?>
                    <a class="nav-link" href="/logout.php">Logout</a>
                   <?php }else { ?>
                        <a class="nav-link" href="#" data-toggle="modal" data-target="#login">Login</a>
                        <a class="nav-link" href="#" data-toggle="modal" data-target="#register">Register</a>
                   <?php } ?>


            </nav>
        </div>
    </div>
</div>

<!-- Modal Login -->
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Login</h4>
            </div>
            <div class="modal-body">
                <form action='/login.php' method="POST">
                    <div class="form-group">
                        <label for="username">username</label>
                        <input type="text" class="form-control" name='username' id="username" required>
                    </div>
                    <div class="form-group">
                        <label for="password">password</label>
                        <input type='password' class="form-control" name='password' required>
                    </div>
                    <input type="submit" name='logIn' class='btn btn-outline-info' value="Login">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal Register -->
<div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Register</h4>
            </div>
            <div class="modal-body">
                <form action='/register.php' method="POST">
                    <div class="form-group">
                        <label for="username">username</label>
                        <input type="text" class="form-control" name='username' id="username" required>
                    </div>
                    <div class="form-group">
                        <label for="password">password</label>
                        <input type='password' class="form-control" name='password' required>
                    </div>
                    <div class="form-group">
                        <label for="passwordconfirm">confirm password</label>
                        <input type='password' class="form-control" name='passwordconfirm' required>
                    </div>
                    <input type="submit" name='register' class='btn btn-outline-info' value="Login">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </form>
            </div>
        </div>
    </div>
</div>