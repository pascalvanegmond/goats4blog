<!-- Modal update -->
<div class="modal fade" id="update<?= $comment['id']?>" tabindex="-1" role="dialog" aria-labelledby="updateComment" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Edit your comment</h4>
            </div>
            <div class="modal-body">
                <form action='/comment/edit.php' method="POST">
                    <div class="input-group">
                        <div class="col-md-8">
                            <textarea class="form-control" name="comment" rows="5" required ><?= $comment['comment']?></textarea>
                        </div>
                        <div class="col-md-4"><br>
                            <input type="submit" name='AddComment' class='btn btn-outline-info btn-lg' value="Submit" >
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal delete -->
<div class="modal fade" id="delete<?= $comment['id']?>" tabindex="-1" role="dialog" aria-labelledby="deleteComment" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Are you sure you want to delete this comment?</h4>
            </div>
            <div class="modal-body">
                <form action='/comment/delete.php' method="POST">
                    <input type="submit" name='deleteComment' class='btn btn-outline-info' value="Yes">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No!</button>
                </form>
            </div>
        </div>
    </div>
</div>