<?php
session_start();

/* Connect to a MySQL database using driver invocation */
$dsn = 'mysql:dbname=goats4blog;host=127.0.0.1';
$user = 'root';
$password = '';

try {
    $dbh = new PDO($dsn, $user, $password);
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}

if(isset($lvl)&& $lvl == 1){
    $lvl = '.';
}else{
    $lvl = '';
}
require $lvl . './classes/auth.php';
$auth = new Auth($dbh);

require $lvl . './classes/blog.php';
$blog = new Blog($dbh);

require $lvl . './classes/comment.php';
$comment = new Comment($dbh);

require $lvl . './classes/category.php';
$category = new Category($dbh);

?>