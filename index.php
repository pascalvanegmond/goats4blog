<?php
require_once 'include/connection.php';


// Find out how many items are in the table
$total = $blog->getAmountOfBlogPosts();

// How many items to list per page
$limit = 2;

// How many pages will there be
$pages = ceil($total / $limit);

// get the current page.
if(isset($_GET['page'])){
    $page = (int)$_GET['page'];
}else{
    $page = 1;
}

// Calculate the offset for the query
$offset = ($page - 1)  * $limit;

// Some information to display to the user
$start = $offset + 1;
$end = min(($offset + $limit), $total);

if(isset($_GET['page']) && $_GET['page'] > $pages){
    header('Location: ./index.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Goats4Blog</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.4/css/bootstrap.min.css" integrity="sha384-2hfp1SzUoho7/TsGGGDaFdsuuDL0LX2hnUp6VkX3CUQ2K4K+xjboZdsXyp4oUHZj" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="css/blog.css" rel="stylesheet">
</head>

<body>
<!-- navbar-->
<?php include 'include/navbar.php'?>


<div class="blog-header">
    <div class="container">
        <?php  if(isset($_SESSION['error'])){ ?>
            <div class="alert alert-danger" role="alert"><strong>Oh snap!</strong> <?= $_SESSION['error'] ?></div>
            <?php  unset($_SESSION['error']);
        } ?>
        <?php  if(isset($_SESSION['success'])){ ?>
            <div class="alert alert-success" role="alert"><strong><i class="fa fa-check-circle-o" aria-hidden="true"></i></strong> <?= $_SESSION['success'] ?></div>
            <?php  unset($_SESSION['success']);
        } ?>
        <h1 class="blog-title">Welcome to Goat4Blog</h1>
        <p class="lead blog-description">A blog of the intergalactic virtual epic goats of happiness.</p>
    </div>
</div>

<div class="container">

    <div class="row">

        <div class="col-sm-8 blog-main">

            <?php

            $posts = $blog->getAllBlogPosts($limit, $offset);
            $num_comments = 0;

            foreach ($posts as $post){
                $num_comments = $comment->getNumberOfComments($post['id']);
                $post['created_at'] = strtotime($post['created_at']);
            ?>

            <div class="blog-post">
                <div class="row">
                    <div class="col-md-8">
                        <h2 class="blog-post-title"><?= $post['title'] ?></h2>
                    </div>
                    <div class="col-md-4">
                        <?php if(isset($_SESSION['can_write']) && $_SESSION['can_write'] == 1){   ?>
                            <a href="./post/edit.php?post=<?= $post['id'] ?>"> <i class="fa fa-pencil pull-right" data-toggle="tooltip" data-placement="top" title="Edit this post" aria-hidden="true"></i></a><br>
                            <a href="./post/delete.php?post=<?= $post['id'] ?>"> <i class="fa fa-trash  pull-right" data-toggle="tooltip" data-placement="bottom" title="Delete this post" aria-hidden="true"></i></a>
                        <?php  } ?>
                    </div>
                </div>
                <p class="blog-post-meta"><?= date("F d, Y",$post['created_at']) ?> by <a href="#"><?= $post['username'] ?> </a> <span class="pull-right"> <small>category: &nbsp;</small> <?= $post['category'] ?> </span></p>
                <?= $post['content'] ?>
                <div class="sidebar-module">
                    <img src="<?= $post['photo'] ?>" class="img-rounded img-fluid" style="max-width: 100%;">
                </div>
                <p><a href="./post/show.php?post=<?= $post['id'] ?>">Read more ...</a> </p>
                <p> <a href="#"></a><i class="fa fa-commenting-o fa-lg" aria-hidden="true"> </i> <?= $num_comments  ?> </p>

            </div><!-- /.blog-post -->

            <?php  } ?>

            <nav class="blog-pagination">
                <a class="btn <?php if($page <= 1){ echo 'btn-outline-secondary disabled'; }else{ echo 'btn-outline-primary';} ?>" href="?page=<?= ($page - 1) ?>">Newer</a>
                <a class="btn <?php if($page >= $pages){ echo 'btn-outline-secondary disabled'; }else{ echo 'btn-outline-primary';} ?>" href="?page=<?= ($page + 1) ?>">Older</a>
            </nav>

        </div><!-- /.blog-main -->

        <div class="col-sm-3 offset-sm-1 blog-sidebar">
            <div class="sidebar-module sidebar-module-inset">
                <h4>About</h4>
                <p>Goats4Blog <em>is the first and unique blog dedicated to goats.</em> Our goal is to post as many intergalactic virtual goats of happiness.</p>
            </div>
            <div class="sidebar-module">
                <img src="img/goat1.jpg" alt="cute" class="img-rounded pull-xs-right" style="max-width: 200px;">
            </div>
            <div class="sidebar-module more-margin">
                <h4>Recent Posts</h4>
                <ol class="list-unstyled">
                <?php
                    $posts = $blog->getAllBlogPosts(10, 0);
                    foreach ($posts as $post) { ?>
                        <li><a href="/post/show.php?post=<?= $post['id'] ?>"><?=$post['title']?></a></li>
                <?php } ?>
                </ol>
            </div>
            <div class="sidebar-module">
                <h4>Elsewhere</h4>
                <ol class="list-unstyled">
                    <li><a href="https://github.com/pascalvanegmond" target="_blank">GitHub</a></li>
                    <li><a href="https://bitbucket.org/verzinzelf/" target="_blank">Bitbucket</a></li>
                    <li><a href="#">Facebook</a></li>
                </ol>
            </div>
        </div><!-- /.blog-sidebar -->

    </div><!-- /.row -->

</div><!-- /.container -->

<footer class="blog-footer">
<!--    <p>Blog template built for <a href="http://getbootstrap.com">Bootstrap</a> by <a href="https://twitter.com/mdo">@mdo</a>.</p>-->
<!--    <p>-->
        <a href="#">Back to top</a>
    </p>
</footer>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js" integrity="sha384-THPy051/pYDQGanwU6poAc/hOdQxjnOEXzbT+OuUAFqNqFjL+4IGLBgCJC3ZOShY" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.2.0/js/tether.min.js" integrity="sha384-Plbmg8JY28KFelvJVai01l8WyZzrYWG825m+cZ0eDDS1f7d/js6ikvy1+X+guPIB" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.4/js/bootstrap.min.js" integrity="sha384-VjEeINv9OSwtWFLAtmc4JCtEJXXBub00gtSnszmspDLCtC0I4z4nqz7rEFbIZLLU" crossorigin="anonymous"></script>
<!-- Font awesome icons-->
<script src="https://use.fontawesome.com/8eb0b591f3.js"></script>

<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>

</body>
</html>



