<?php
$lvl = 1;
require_once '../include/connection.php';

// Basic check if the user is logged in
if(!isset($_SESSION['username'])){
    $_SESSION['error'] = 'You have to log in first';
    header('Location: ../index.php');
}


// Get the $_GET with the specific blog_id.
if(isset($_GET['post'])){
    $blogPost = $blog->getBlogPostById($_GET['post']);

    // if there is no post found redirect with error.
    if($blogPost === flase){
        $_SESSION['error'] = 'Sorry we couldn\'t find that specific blog post! ';
        header('Location: ../index.php');
    }else{
        if($blog->deleteBlogPostById($_GET['post'])){
            $_SESSION['success'] = 'Post successfully deleted';
            header('Location: ../index.php');
        }else{
            $_SESSION['error'] = 'Sorry we couldn\'t delete that post!';
            header('Location: ../index.php');
        }
    }
}



// Check if the form is submitted. and set variables.
if(isset($_POST['deleteComment'])){
    $writer = $_SESSION['username'];
    $blog_id = $_SESSION['post'];
    $id = $_SESSION['comment_id'];

    // Call function updateBlogPost to update the blog post with the parameters. When it returns true it will redirect you to the main page.
    if($comment->deleteCommentById($blog_id, $writer, $id)){
        unset($_SESSION['post']);
        unset($_SESSION['comment_id']);

        header('Location: /post/show.php?post='. $blog_id);
    }
}


?>