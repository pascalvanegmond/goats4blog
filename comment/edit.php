<?php
$lvl = 1;
require_once '../include/connection.php';

// Basic check if the user is logged in
if(!isset($_SESSION['username'])){
    $_SESSION['error'] = 'You have to log in first';
    header('Location: ../index.php');
}

// Check if the form is submitted. and set variables.
if(isset($_POST['AddComment'])){
    $content = $_POST['comment'];
    $writer = $_SESSION['username'];
    $blog_id = $_SESSION['post'];
    $id = $_SESSION['comment_id'];


    // Call function updateComment to update the comment with the parameters. When it returns true it will redirect you to the blog page.
    if($comment->updateComment($content,$writer,$id)){
        unset($_SESSION['post']);
        unset($_SESSION['comment_id']);

        header('Location: /post/show.php?post='. $blog_id);
    }
}

?>