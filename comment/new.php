<?php
$lvl = 1;
require_once '../include/connection.php';

// Basic check if the user is logged in
if(!isset($_SESSION['username'])){
    $_SESSION['error'] = 'You have to log in first';
    header('Location: ../index.php');
}

if(isset($_POST['AddComment'])){
    $content = $_POST['comment'];
    $writer = $_SESSION['username'];
    $blog_id = $_SESSION['post'];

    if($comment->addComment($content, $writer, $blog_id)){

        header('Location: /post/show.php?post='. $blog_id);
    }

}

?>