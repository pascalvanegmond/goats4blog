<?php
Class Category{

    private $_db;

    function __construct($dbh)
    {

        $this->_db = $dbh;
    }

    public function getAllCategories()
    {
        $stmt = $this->_db->prepare("SELECT * 
                                      FROM category
                                      ORDER BY id DESC");
        $stmt->execute();

        $categories = $stmt->fetchall();

        return $categories;

    }

    public function createCategory($category)
    {
        $stmt = $this->_db->prepare("INSERT INTO category (category) 
                                            VALUES (
                                                :category)");
        $stmt->bindParam(':category', $category, PDO::PARAM_STR);
        $stmt->execute();;

        return True;
    }
}