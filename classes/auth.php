<?php
Class Auth{

    private $_db;

    function __construct($dbh){

        $this->_db = $dbh;
    }

    public function login($username,$password)
    {
        $stmt = $this->_db->prepare('SELECT password, is_admin FROM users WHERE username = :username ');
        $stmt->execute(array('username' => $username));

        $row = $stmt->fetch();
        if($row['password'] == hash('sha256', $password)){
            $_SESSION['loggedin'] = true;
            $_SESSION['username'] = $username;
            $_SESSION['can_write'] = $row['is_admin'];
            return true;
        }
    }

    public function logout(){
        session_destroy();
    }


    public function register($username, $password)
    {
            $stmt = $this->_db->prepare('INSERT INTO users (username, password) VALUES (:username, :password) ');
            $stmt->execute(array(
                'username' => $username,
                'password' => hash('sha256', $password)
            ));
            $_SESSION['loggedin'] = true;
            $_SESSION['username'] = $username;
            return true;

    }
}
?>