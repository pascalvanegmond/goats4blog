<?php
Class Comment{

    private $_db;

    function __construct($dbh)
    {

    $this->_db = $dbh;
    }

    public function getNumberOfComments($blog_id)
    {
        $stmt = $this->_db->prepare("SELECT COUNT(comment) AS totcomm 
                                      FROM `comments` 
                                      WHERE blog_id = :blog_id");
        $stmt->bindParam(':blog_id', $blog_id, PDO::PARAM_INT);
        $stmt->execute();

        $number = $stmt->fetchColumn();

        return $number;
    }

    public function getAllCommentsByPost($blog_id)
    {
        $stmt = $this->_db->prepare("SELECT comments.id, comments.comment, comments.created_at, users.username 
                                      FROM `comments` 
                                          LEFT JOIN users
                                            ON comments.users_id=users.id
                                      WHERE blog_id = :blog_id");
        $stmt->bindParam(':blog_id', $blog_id, PDO::PARAM_INT);
        $stmt->execute();

        $comments = $stmt->fetchAll();

        return $comments;
    }

    public function addComment($content, $writer, $blog_id)
    {
        $stmt = $this->_db->prepare("INSERT INTO comments (comment, users_id, blog_id) 
                                            VALUES (
                                                :comment,
                                                (SELECT id from users where username = :writer),
                                                :blog_id)");
        $stmt->bindParam(':comment', $content, PDO::PARAM_STR);
        $stmt->bindParam(':writer', $writer, PDO::PARAM_INT);
        $stmt->bindParam(':blog_id', $blog_id, PDO::PARAM_INT);
        $stmt->execute();

        return True;
    }

    public function updateComment($content,$writer,$id)
    {
        $stmt = $this->_db->prepare("UPDATE comments 
                                        SET comment = :comment
                                        WHERE
                                        id = :id
                                        AND 
                                        users_id = (SELECT id from users where username = :writer)");
        $stmt->bindParam(':comment', $content, PDO::PARAM_STR);
        $stmt->bindParam(':writer', $writer, PDO::PARAM_STR);
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        return True;
    }

    public function deleteCommentById($blog_id, $writer, $id)
    {
        $stmt = $this->_db->prepare("DELETE FROM comments 
                                      WHERE 
                                      blog_id = :blog_id
                                      AND
                                      users_id = (SELECT id from users where username = :writer)
                                      AND 
                                      id =  :id");
        $stmt->bindParam(':blog_id', $blog_id, PDO::PARAM_STR);
        $stmt->bindParam(':writer', $writer, PDO::PARAM_STR);
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        return True;
    }
}

