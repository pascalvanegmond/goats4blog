<?php
Class Blog{

    private $_db;

    function __construct($dbh)
    {

        $this->_db = $dbh;
    }

    public function getAmountOfBlogPosts()
    {
        $stmt = $this->_db->prepare('SELECT COUNT(*)
                                      FROM blog');
        $stmt->execute();
        $amount = $stmt->fetchColumn();

        return $amount;
    }

    public function getAllBlogPosts($limit, $offset)
    {
        $stmt = $this->_db->prepare("SELECT blog.id, blog.title, blog.content, blog.photo, blog.created_at, users.username, category.category
                                      FROM `blog`
                                        LEFT JOIN users
                                          ON blog.users_id=users.id
                                        LEFT JOIN category
                                          ON blog.category_id=category.id
                                        ORDER BY blog.id DESC
                                        LIMIT
                                            :limit
                                        OFFSET
                                            :offset");
        $stmt->bindParam(':limit', $limit, PDO::PARAM_INT);
        $stmt->bindParam(':offset', $offset, PDO::PARAM_INT);
        $stmt->execute();

        $posts = $stmt->fetchall();

        return $posts;
    }

    public function getBlogPostById($id)
    {
        $stmt = $this->_db->prepare("SELECT blog.id, blog.title, blog.content, blog.photo, blog.created_at, users.username, category.category 
                                      FROM `blog` 
                                        LEFT JOIN users
                                          ON blog.users_id=users.id 
                                        LEFT JOIN category
                                          ON blog.category_id=category.id
                                      Where blog.id = :id");
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $post = $stmt->fetch();

        $post['created_at'] = strtotime($post['created_at']);

        return $post;
    }


    public function addBlogPost($title,$content,$photo,$writer,$category)
    {
        $stmt = $this->_db->prepare("INSERT INTO blog (title, content, photo, users_id, category_id) 
                                        VALUES (
                                            :title,
                                            :content,
                                            :photo,
                                            (SELECT id from users where username = :writer),
                                            :category_id)");
        $stmt->bindParam(':title', $title, PDO::PARAM_STR);
        $stmt->bindParam(':content', $content, PDO::PARAM_STR);
        $stmt->bindParam(':photo', $photo, PDO::PARAM_STR);
        $stmt->bindParam(':writer', $writer, PDO::PARAM_STR);
        $stmt->bindParam(':category_id', $category, PDO::PARAM_INT);
        $stmt->execute();

        return True;
    }

    public function updateBlogPost($title,$content,$photo,$category, $id)
    {
        $stmt = $this->_db->prepare("UPDATE blog 
                                        SET title = :title,
                                            content = :content,
                                            photo = :photo,
                                            category_id = :category_id
                                        WHERE
                                        id = :id");
        $stmt->bindParam(':title', $title, PDO::PARAM_STR);
        $stmt->bindParam(':content', $content, PDO::PARAM_STR);
        $stmt->bindParam(':photo', $photo, PDO::PARAM_STR);
        $stmt->bindParam(':category_id', $category, PDO::PARAM_INT);
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        return True;
    }

    public function deleteBlogPostById($id)
    {
        $stmt = $this->_db->prepare("DELETE FROM blog WHERE id =  :id");
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $countDel = $stmt->rowCount();
        if ($countDel == 0) {
           return False;
        }

        return True;
    }


}