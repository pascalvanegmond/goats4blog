<?php
$lvl = 1;
require_once '../include/connection.php';

// Basic check if the user is logged in
if(!isset($_SESSION['username'])){
    $_SESSION['error'] = 'You have to log in first';
    header('Location: ../index.php');
}

// Check if the users is an admin/author.
if(isset($_SESSION['can_write']) && $_SESSION['can_write'] != 1){
    $_SESSION['error'] = 'You don\'t have premision!';
    header('Location: ../index.php');
}

// Get the $_GET with the specific blog_id.
if(isset($_GET['post'])){
    $blogPost = $blog->getBlogPostById($_GET['post']);
    $_SESSION['post'] = $_GET['post'];

    // if there is no post found redirect with error.
    if($blogPost === false){
        $_SESSION['error'] = 'Sorry we couldn\'t find that specific blog post! ';
        header('Location: ../index.php');
    }
}

// Check if the form is submitted. and set variables.
if(isset($_POST['updatePost'])){
    $title = $_POST['title'];
    $photo = $_POST['photo'];
    $content = $_POST['content'];
    $category = $_POST['category'];
    $id = $_SESSION['post'];

    // Call function updateBlogPost to update the blog post with the parameters. When it returns true it will redirect you to the main page.
    if($blog->updateBlogPost($title,$content,$photo,$category,$id)){
        unset($_SESSION['post']);
        $_SESSION['success'] = 'The post: "' . $title . '" has been updated!';
        header('Location: ../index.php');
    }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Goats4Blog</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.4/css/bootstrap.min.css" integrity="sha384-2hfp1SzUoho7/TsGGGDaFdsuuDL0LX2hnUp6VkX3CUQ2K4K+xjboZdsXyp4oUHZj" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="../css/blog.css" rel="stylesheet">
</head>

<body>
<!-- navbar-->
<?php include '../include/navbar.php'?>

<div class="blog-header">
    <div class="container">
        <h1 class="blog-title">Welcome to Goat4Blog</h1>
        <p class="lead blog-description">A blog of the intergalactic virtual epic goats of happiness.</p>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-sm-8 blog-main">
            <div class="blog-post">
                <form action='edit.php' method="POST">
                    <div class="form-group">
                        <label for="category">Category</label>
                        <select class="form-control" id="category" name="category">
                            <?php
                            $categories = $category->getAllCategories();
                            foreach ($categories as $category){  ?>
                                <option value="<?= $category['id']?>" <?php if($category['category'] == $blogPost['category']){ echo 'selected'; } ?>><?= $category['category']?></option>
                            <?php  } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="title">Blog Title</label>
                        <input type="text" class="form-control" name='title' id="title" value="<?= $blogPost['title'] ?>" required>
                    </div>
                    <div class="form-group">
                        <label for="photo">Blog photo</label>
                        <input type='text' class="form-control" name='photo' value="<?= $blogPost['photo'] ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleTextarea">Content of blog <small>(You can use basic html)</small></label>
                        <textarea class="form-control" name="content" rows="8" required><?= $blogPost['content'] ?></textarea>
                    </div>
                    <input type="submit" name='updatePost' class='btn btn-outline-info' value="Update">
                </form>

            </div><!-- /.blog-post -->


        </div><!-- /.blog-main -->

        <div class="col-sm-3 offset-sm-1 blog-sidebar">
            <div class="sidebar-module sidebar-module-inset">
                <h4>About</h4>
                <p>Goats4Blog <em>is the first and unique blog dedicated to goats.</em> Our goal is to post as many intergalactic virtual goats of happiness.</p>
            </div>
            <div class="sidebar-module">
                <img src="../img/goat1.jpg" alt="cute" class="img-rounded pull-xs-right" style="max-width: 200px;">
            </div>
            <div class="sidebar-module more-margin">
                <h4>Archives</h4>
                <ol class="list-unstyled">
                    <?php
                    $posts = $blog->getAllBlogPosts(10, 0);
                    foreach ($posts as $post) { ?>
                        <li><a href="../post/show.php?post=<?= $post['id'] ?>"><?=$post['title']?></a></li>
                    <?php } ?>
                </ol>
            </div>
            <div class="sidebar-module">
                <h4>Elsewhere</h4>
                <ol class="list-unstyled">
                    <li><a href="https://github.com/pascalvanegmond" target="_blank">GitHub</a></li>
                    <li><a href="https://bitbucket.org/verzinzelf/" target="_blank">Bitbucket</a></li>
                    <li><a href="#">Facebook</a></li>
                </ol>
            </div>
        </div><!-- /.blog-sidebar -->

    </div><!-- /.row -->

</div><!-- /.container -->

<footer class="blog-footer">
<!--    <p>Blog template built for <a href="http://getbootstrap.com">Bootstrap</a> by <a href="https://twitter.com/mdo">@mdo</a>.</p>-->
<!--    <p>-->
        <a href="#">Back to top</a>
    </p>
</footer>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js" integrity="sha384-THPy051/pYDQGanwU6poAc/hOdQxjnOEXzbT+OuUAFqNqFjL+4IGLBgCJC3ZOShY" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.2.0/js/tether.min.js" integrity="sha384-Plbmg8JY28KFelvJVai01l8WyZzrYWG825m+cZ0eDDS1f7d/js6ikvy1+X+guPIB" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.4/js/bootstrap.min.js" integrity="sha384-VjEeINv9OSwtWFLAtmc4JCtEJXXBub00gtSnszmspDLCtC0I4z4nqz7rEFbIZLLU" crossorigin="anonymous"></script>
<!-- Font awesome, icons-->
<script src="https://use.fontawesome.com/8eb0b591f3.js"></script>

<!-- this will remove the active class from previously active menu item -->
<script>
    $(document).ready(function () {
        $(".nav-link").removeClass("active");
        $('#post').addClass('active');
    });
</script>

</body>
</html>



