<?php
$lvl = 1;
require_once '../include/connection.php';

// Basic check if the user is logged in
if(!isset($_SESSION['username'])){
    $_SESSION['error'] = 'You have to log in first';
    header('Location: ../index.php');
}

// Check if the users is an admin/author.
if(isset($_SESSION['can_write']) && $_SESSION['can_write'] != 1){
    $_SESSION['error'] = 'You don\'t have premision!';
    header('Location: ../index.php');
}

// Get the $_GET with the specific blog_id.
if(isset($_GET['post'])){
    $blogPost = $blog->getBlogPostById($_GET['post']);

    // if there is no post found redirect with error.
    if($blogPost === false){
        $_SESSION['error'] = 'Sorry we couldn\'t find that specific blog post! ';
        header('Location: ../index.php');
    }else{
        if($blog->deleteBlogPostById($_GET['post'])){
            $_SESSION['success'] = 'Post successfully deleted';
            header('Location: ../index.php');
        }else{
            $_SESSION['error'] = 'Sorry we couldn\'t delete that post!';
            header('Location: ../index.php');
        }
    }
}


?>