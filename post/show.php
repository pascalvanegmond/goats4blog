<?php
$lvl = 1;
require_once '../include/connection.php';

// Get the $_GET with the specific blog_id.
if(isset($_GET['post'])){
    $blogPost = $blog->getBlogPostById($_GET['post']);
    $_SESSION['post'] = $_GET['post'];

    // if there is no post found redirect with error.
    if($blogPost === false){
        $_SESSION['error'] = 'Sorry we couldn\'t find that specific blog post! ';
        header('Location: ../index.php');
    }
}
if(isset($_POST['comment_id'])){
    $_SESSION['comment_id'] = substr($_POST['comment_id'], 7); // remove #update or #delete to get the comment id.
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Goats4Blog</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.4/css/bootstrap.min.css" integrity="sha384-2hfp1SzUoho7/TsGGGDaFdsuuDL0LX2hnUp6VkX3CUQ2K4K+xjboZdsXyp4oUHZj" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="../css/blog.css" rel="stylesheet">
    <link href="../css/comments.css" rel="stylesheet">
</head>

<body>
<!-- navbar-->
<?php include '../include/navbar.php'?>

<div class="blog-header">
    <div class="container">
        <h1 class="blog-title">Welcome to Goat4Blog</h1>
        <p class="lead blog-description">A blog of the intergalactic virtual epic goats of happiness.</p>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-sm-8 blog-main">
            <div class="blog-post">
                <div class="row">
                    <div class="col-md-8">
                        <h2 class="blog-post-title"><?= $blogPost['title'] ?></h2>
                    </div>
                    <div class="col-md-4">
                        <?php if(isset($_SESSION['can_write']) && $_SESSION['can_write'] == 1){   ?>
                            <a href="./edit.php?post=<?= $blogPost['id'] ?>"> <i class="fa fa-pencil pull-right" data-toggle="tooltip" data-placement="top" title="Edit this post" aria-hidden="true"></i></a><br>
                            <a href="./delete.php?post=<?= $blogPost['id'] ?>"> <i class="fa fa-trash  pull-right" data-toggle="tooltip" data-placement="bottom" title="Delete this post" aria-hidden="true"></i></a>
                        <?php  } ?>
                    </div>
                </div>
                <p class="blog-post-meta"><?= date("F d, Y",$blogPost['created_at'])?> by <a href="#"><?= $blogPost['username'] ?></a></p>
                <?= $blogPost['content'] ?>
            </div><!-- /.blog-post -->

            <div class="sidebar-module">
                <img src="<?= $blogPost['photo'] ?>" class="img-rounded img-fluid" style="max-width: 100%;">
            </div>

            <div class="blog-post">
                <ul class="list-group">
                    <li class="list-group-item">
                        <form action='../comment/new.php' method="POST">
                            <div class="input-group">
                                <label for="exampleTextarea"><?php if(!isset($_SESSION['username'])){ echo "You have to be <a href=\"#\" data-toggle=\"modal\" data-target=\"#login\">logged</a> in to post a comment"; } else { echo 'Do you have something to say about this post?';};?></label>
                                <div class="col-md-8">
                                    <textarea class="form-control" name="comment" rows="5" required <?php if(!isset($_SESSION['username'])){ echo 'disabled'; };?>></textarea>
                                </div>
                                <div class="col-md-4"><br>
                                    <input type="submit" name='AddComment' class='btn btn-outline-info btn-lg' value="Submit" <?php if(!isset($_SESSION['username'])){ echo 'disabled'; };?>>
                                </div>
                            </div>
                        </form>
                    </li>
                    <li class="list-group-item">

                        <?php
                        $comments = $comment->getAllCommentsByPost($_GET['post']);
                        foreach($comments as $comment){
                            $date = strtotime($comment['created_at']);
                            include '../include/_modal_edit_comment.php'
                            ?>
                            <div class="dialogbox">
                                <p class="comment pull-left"> <?= date("d-M-Y",$date)?></p>
                                <div class="body">
                                    <span class="tip tip-left"></span>
                                    <div class="message">
                                        <span><span class="pull-left"><?= date("l H:i:s",$date)?> by <a href="#"> <?= $comment['username']?></a></span>
                                            <?php if(isset($_SESSION['username']) && $_SESSION['username'] == $comment['username']){   ?>
                                                <a href="#" data-toggle="modal" data-target="#update<?= $comment['id']?>"> <i class="fa fa-pencil pull-right" data-toggle="tooltip" data-placement="top" title="Edit this post" aria-hidden="true"></i></a>
                                                <a href="#" data-toggle="modal" data-target="#delete<?= $comment['id']?>"> <i class="fa fa-trash  pull-right" data-toggle="tooltip" data-placement="bottom" title="Delete this post" aria-hidden="true"></i></a>
                                            <?php  } ?>
                                            <br><?= $comment['comment']?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                       <?php  }     ?>
                    </li>
                </ul>
            </div>
        </div><!-- /.blog-main -->





        <div class="col-sm-3 offset-sm-1 blog-sidebar">
            <div class="sidebar-module sidebar-module-inset">
                <h4>About</h4>
                <p>Goats4Blog <em>is the first and unique blog dedicated to goats.</em> Our goal is to post as many intergalactic virtual goats of happiness.</p>
            </div>
            <div class="sidebar-module">
                <img src="../img/goat1.jpg" alt="cute" class="img-rounded pull-xs-right" style="max-width: 200px;">
            </div>
            <div class="sidebar-module more-margin">
                <h4>Archives</h4>
                <ol class="list-unstyled">
                    <?php
                    $posts = $blog->getAllBlogPosts(10, 0);
                    foreach ($posts as $post) { ?>
                        <li><a href="/post/show.php?post=<?= $post['id'] ?>"><?=$post['title']?></a></li>
                    <?php } ?>
                </ol>
            </div>
            <div class="sidebar-module">
                <h4>Elsewhere</h4>
                <ol class="list-unstyled">
                    <li><a href="https://github.com/pascalvanegmond" target="_blank">GitHub</a></li>
                    <li><a href="https://bitbucket.org/verzinzelf/" target="_blank">Bitbucket</a></li>
                    <li><a href="#">Facebook</a></li>
                </ol>
            </div>
        </div><!-- /.blog-sidebar -->

    </div><!-- /.row -->

</div><!-- /.container -->

<footer class="blog-footer">
<!--    <p>Blog template built for <a href="http://getbootstrap.com">Bootstrap</a> by <a href="https://twitter.com/mdo">@mdo</a>.</p>-->
<!--    <p>-->
        <a href="#">Back to top</a>
    </p>
</footer>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js" integrity="sha384-THPy051/pYDQGanwU6poAc/hOdQxjnOEXzbT+OuUAFqNqFjL+4IGLBgCJC3ZOShY" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.2.0/js/tether.min.js" integrity="sha384-Plbmg8JY28KFelvJVai01l8WyZzrYWG825m+cZ0eDDS1f7d/js6ikvy1+X+guPIB" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.4/js/bootstrap.min.js" integrity="sha384-VjEeINv9OSwtWFLAtmc4JCtEJXXBub00gtSnszmspDLCtC0I4z4nqz7rEFbIZLLU" crossorigin="anonymous"></script>
<!-- Font awesome, icons-->
<script src="https://use.fontawesome.com/8eb0b591f3.js"></script>

<!-- this will remove the active class from previously active menu item -->
<script>
    $(document).ready(function () {
        $(".nav-link").removeClass("active");

        $("*[data-target]").click(function()
        {
            var src = $(this).attr("data-target");
            $.post("/post/show.php", {"comment_id": src});
        });
    });
</script>

<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>

</body>
</html>



